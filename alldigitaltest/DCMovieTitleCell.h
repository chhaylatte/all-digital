//
//  DCMovieResultCell.h
//  alldigitaltest
//
//  Created by Danny Chhay on 3/30/14.
//  Copyright (c) 2014 Danny Chhay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCMovieTitleCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UILabel *contentAdvisoryRatingLabel;
@property (nonatomic, strong) IBOutlet UILabel *releaseDateLabel;
@property (nonatomic, strong) IBOutlet UIImageView *imView;
@property (nonatomic, strong) IBOutlet UIButton *buyButton;

@end

//
//  DCMovieSearchControllerViewController.h
//  alldigitaltest
//
//  Created by Danny Chhay on 3/30/14.
//  Copyright (c) 2014 Danny Chhay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCMovieSearchControllerViewController : UIViewController <UISearchBarDelegate, NSURLConnectionDelegate>

// IBOutlets
@property (nonatomic, strong) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *actInd;

@end

//
//  DCMovieDetailViewController.h
//  alldigitaltest
//
//  Created by Danny Chhay on 3/30/14.
//  Copyright (c) 2014 Danny Chhay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@class DCMovieTitleCell, DCMovieDetailsCell;

@interface DCMovieDetailViewController : UITableViewController

@property (nonatomic, copy) NSDictionary *movieDetails;

// IBOutlets
@property (nonatomic, strong) IBOutlet DCMovieTitleCell *titleCell;
@property (nonatomic, strong) IBOutlet DCMovieDetailsCell *detailCell;
@property (nonatomic, strong) IBOutlet UITableViewCell *previewCell;
// TitleCell
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UILabel *contentAdvisoryRatingLabel;
@property (nonatomic, strong) IBOutlet UILabel *releaseDateLabel;
@property (nonatomic, strong) IBOutlet UIImageView *imView;
@property (nonatomic, strong) IBOutlet UIButton *buyButton;
// DetailCell
@property (nonatomic, strong) IBOutlet UILabel *detailHeaderLabel;
@property (nonatomic, strong) IBOutlet UITextView *detailTextView;

// IBActions
-(IBAction)pushButton:(id)sender;

@end

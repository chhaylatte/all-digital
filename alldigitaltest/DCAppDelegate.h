//
//  DCAppDelegate.h
//  alldigitaltest
//
//  Created by Danny Chhay on 3/30/14.
//  Copyright (c) 2014 Danny Chhay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

//
//  main.m
//  alldigitaltest
//
//  Created by Danny Chhay on 3/30/14.
//  Copyright (c) 2014 Danny Chhay. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DCAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DCAppDelegate class]));
    }
}

//
//  DCMovieSearchControllerViewController.m
//  alldigitaltest
//
//  Created by Danny Chhay on 3/30/14.
//  Copyright (c) 2014 Danny Chhay. All rights reserved.
//

#import "DCMovieSearchControllerViewController.h"

#import "DCMovieResultsViewController.h"

@interface DCMovieSearchControllerViewController ()

@property (nonatomic, strong, readonly) NSMutableURLRequest *searchRequest;
@property (nonatomic, strong) NSURLConnection *urlConnection;
@property (nonatomic, strong, readonly) NSMutableData *receivedData;


@end

@implementation DCMovieSearchControllerViewController

@synthesize receivedData = _receivedData;
@synthesize searchRequest = _searchRequest;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([sender isKindOfClass:[NSArray class]] && [segue.destinationViewController isKindOfClass:[DCMovieResultsViewController class]]) {
        ((DCMovieResultsViewController*)segue.destinationViewController).movieResults = sender;
    }
}

#pragma mark - Setters & Getters

-(NSMutableData*)receivedData
{
    if (!_receivedData) {
        _receivedData = [NSMutableData new];
    }
    return _receivedData;
}

-(NSMutableURLRequest*)searchRequest
{
    if (!_searchRequest) {
        //! In a real app, use caching
        //! This the API endpoint.  Pass in an actual search term to the url
        _searchRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"https://itunes.apple.com/search?term=searchTerm&media=movie"] cachePolicy:NSURLCacheStorageNotAllowed timeoutInterval:60];
    }
    return _searchRequest;
}

-(void)searchMovie:(NSString*)title
{
    // Reset search data
    if (self.urlConnection) {
        [self.urlConnection cancel];
    }
    [self.receivedData setLength:0];
    
    // Create search request and connection with search term
    //! In a real app, user paging
    [self.searchRequest setURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://itunes.apple.com/search?term=%@&media=movie", title]]];
    self.urlConnection = [[NSURLConnection alloc] initWithRequest:self.searchRequest delegate:self startImmediately:NO];
    
    // Start the connection
    [self.actInd startAnimating];
    [self.searchBar resignFirstResponder];
    [self.urlConnection start];
    
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self searchMovie:searchBar.text];
}

#pragma mark - NSURLConnectionDelegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    [self.receivedData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.receivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    self.urlConnection = nil;
    [self.receivedData setLength:0];
    
    NSLog(@"Connection failed! Error - %@ %@",
          
          [error localizedDescription],
          
          [[error userInfo] objectForKey:NSURLErrorFailingURLStringErrorKey]);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error Connecting" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
    [self.actInd stopAnimating];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    self.urlConnection = nil;
    
    NSError *error;
    NSDictionary *jsonData = [NSJSONSerialization JSONObjectWithData:self.receivedData options:0 error:&error];
    
    [self performSegueWithIdentifier:@"showMovieResults" sender:jsonData[@"results"]];
    
    [self.receivedData setLength:0];
    
    [self.actInd stopAnimating];
}


@end

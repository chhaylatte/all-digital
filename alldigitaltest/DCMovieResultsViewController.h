//
//  DCMovieResultsViewController.h
//  alldigitaltest
//
//  Created by Danny Chhay on 3/30/14.
//  Copyright (c) 2014 Danny Chhay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCMovieResultsViewController : UITableViewController

// Movie results returned by https://itunes.apple.com/search?term=%@&media=movie
@property (nonatomic, strong) NSArray *movieResults;

@end

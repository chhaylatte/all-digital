//
//  DCMovieDetailViewController.m
//  alldigitaltest
//
//  Created by Danny Chhay on 3/30/14.
//  Copyright (c) 2014 Danny Chhay. All rights reserved.
//

#import "DCMovieDetailViewController.h"

#import "DCMovieTitleCell.h"
#import "DCMovieDetailsCell.h"

@interface DCMovieDetailViewController ()

@property (nonatomic, strong, readonly) MPMoviePlayerViewController *moviePlayerViewController;

@end

@implementation DCMovieDetailViewController

@synthesize moviePlayerViewController = _moviePlayerViewController;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.movieDetails) {
        // Make date formatter for release date
        NSDateFormatter *readDateFormatter = [[NSDateFormatter alloc] init];
        [readDateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
        NSDate *date = [readDateFormatter dateFromString:self.movieDetails[@"releaseDate"]];
        NSDateFormatter *writeDateFormatter = [[NSDateFormatter alloc] init];
        [writeDateFormatter setDateFormat:@"MM/dd/yyyy"];
        
        // Make currency formatter for buy button
        NSNumberFormatter *currencyFormatter = [[NSNumberFormatter alloc] init];
        [currencyFormatter setCurrencyCode:self.movieDetails[@"currency"]];
        
        NSString *price = [NSNumberFormatter localizedStringFromNumber:[NSNumber numberWithFloat:[self.movieDetails[@"trackPrice"] floatValue]]
                                         numberStyle:NSNumberFormatterCurrencyStyle];
        
        //! Configure the Cells
        // Title Cell
        self.titleLabel.text = self.movieDetails[@"trackName"];
        self.contentAdvisoryRatingLabel.text = [@"Rating - " stringByAppendingString:self.movieDetails[@"contentAdvisoryRating"]];
        self.releaseDateLabel.text = [@"Release Date - " stringByAppendingString:[writeDateFormatter stringFromDate:date]];
        
        //! In a real app, use better image fetching and cache images
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0), ^{
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:self.movieDetails[@"artworkUrl100"]]];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.imView.image = [UIImage imageWithData:imageData];
            });
        });
        
        [self.buyButton setTitle:[price stringByAppendingString:@" - Buy"] forState:UIControlStateNormal];
        // Detail Cell
        self.detailTextView.text = self.movieDetails[@"longDescription"];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)registerNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector: @selector(myMovieFinishedCallback:)
                                                 name: MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.moviePlayerViewController];
}

-(void)myMovieFinishedCallback:(NSNotification*)notification
{
    MPMoviePlayerController* theMovie = [notification object];
    [[NSNotificationCenter defaultCenter]
     removeObserver: self
     name: MPMoviePlayerPlaybackDidFinishNotification
     object: theMovie];
}

#pragma mark - Setters & Getters

-(MPMoviePlayerViewController*)moviePlayerViewController
{
    if (!_moviePlayerViewController) {
        _moviePlayerViewController = [[MPMoviePlayerViewController alloc] initWithContentURL:nil];
    }
    return _moviePlayerViewController;
}

#pragma mark - IBActions

-(void)pushButton:(id)sender
{
    if (sender == self.titleCell.buyButton) {
        //! This does not work on simulator
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.movieDetails[@"trackViewUrl"]]];
    }
}

#pragma mark - UITableViewDelegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //? The play preview row
    if (indexPath.row == 2) {
        self.moviePlayerViewController.moviePlayer.contentURL = [NSURL URLWithString:self.movieDetails[@"previewUrl"]];
        [self.navigationController presentMoviePlayerViewControllerAnimated:self.moviePlayerViewController];
    }
}

#pragma mark - Table view data source

//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//#warning Potentially incomplete method implementation.
//    // Return the number of sections.
//    return 0;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//#warning Incomplete method implementation.
//    // Return the number of rows in the section.
//    return 0;
//}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

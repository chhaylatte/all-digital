//
//  DCMovieDetailsCell.h
//  alldigitaltest
//
//  Created by Danny Chhay on 3/30/14.
//  Copyright (c) 2014 Danny Chhay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DCMovieDetailsCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *detailHeaderLabel;
@property (nonatomic, strong) IBOutlet UITextView *detailTextView;

@end
